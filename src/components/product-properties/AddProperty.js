import React, {Component} from 'react';
import TextField from 'material-ui/TextField';
import {Card, CardActions, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import {connect} from "react-redux";
import * as Actions from "../../actions/productProperties";
import {bindActionCreators} from "redux";


class AddProperty extends Component {

    constructor(props) {
        super(props);

        this.state = {
            inputValue: ''
        };
    }

    render() {
        return (
            <Card>
                <CardText>
                    <TextField
                        hintText="Property Name"
                        value={this.state.inputValue}
                        onChange={event => {
                            const inputValue = event.target.value;
                            this.setState({inputValue});
                        }}
                    />
                </CardText>
                <CardActions>
                    <FlatButton
                        label="Add"
                        onClick={() => {
                            const newMain = {
                                title: this.state.inputValue,
                            };
                            this.props.actions.newMain(newMain);
                        }}
                    />
                </CardActions>
            </Card>
        );
    }
}


const mapStateToProps = state => ({
    productProperties: state.productProperties
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(Actions, dispatch)
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AddProperty);
