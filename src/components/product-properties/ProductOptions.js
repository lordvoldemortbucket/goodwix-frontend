import React, {Component} from 'react';
import SortableTree from 'react-sortable-tree';
import {connect} from "react-redux";
import PropTypes from 'prop-types'
import {bindActionCreators} from 'redux'
import * as Actions from '../../actions/productProperties';
import AddProperty from "./AddProperty";
import ContinueButton from "./ContinueButton";

class ProductOptions extends Component {

    static propTypes = {
        productProperties: PropTypes.array.isRequired,
        actions: PropTypes.object.isRequired
    };

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <div style={{height: 300}}>
                    <SortableTree
                        treeData={this.props.productProperties}
                        onChange={treeData => this.props.actions.setTree(treeData)}
                        generateNodeProps={({node, path}) => {

                            const isMain = path.length === 1;

                            const addButton =
                                <button
                                    onClick={() => {
                                        const newNode = {
                                            title: `new title`,
                                            value: 'new value'
                                        };
                                        this.props.actions.addTodo(path[path.length - 1], newNode);
                                    }}>
                                    Add Child
                                </button>
                            ;
                            const removeButton =
                                <button
                                    onClick={() =>
                                        this.props.actions.delNode(path)
                                    }>
                                    Remove
                                </button>
                            ;
                            const buttons = [removeButton];
                            if (isMain) {
                                buttons.unshift(addButton);
                            }

                            const InputTitle = <input
                                style={{fontSize: '1.1rem'}}
                                value={node.title}
                                onChange={event => {
                                    const title = event.target.value;
                                    const newNode = {...node, title};
                                    this.props.actions.updNode(path, newNode);
                                }}
                            />;

                            const InputValue = <input
                                style={{fontSize: '1.1rem'}}
                                value={node.value}
                                onChange={event => {
                                    const value = event.target.value;
                                    const newNode = {...node, value};
                                    this.props.actions.updNode(path, newNode);
                                }}
                            />;

                            const title = isMain ? InputTitle : (
                                <div>
                                    {InputTitle}
                                    {InputValue}
                                </div>
                            );

                            return {buttons, title};
                        }}
                    />
                </div>
                <ContinueButton />
                <AddProperty/>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    productProperties: state.productProperties,
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(Actions, dispatch),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ProductOptions);
