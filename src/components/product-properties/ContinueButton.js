import React, {Component} from 'react';
import {Card, CardActions} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import {connect} from "react-redux";
import * as Actions from "../../actions/productProperties";
import {bindActionCreators} from "redux";

const ERR_1 = 'Please, fill the form fully.';

class ContinueButton extends Component {

    constructor(props) {
        super(props);

        this.state = {
            inputValue: ''
        };
    }

    render() {
        const {isEnabled} = this.props;
        const info = isEnabled ? null : <div>{ERR_1}</div>;
        return (
            <Card>
                {info}
                <CardActions>
                    <FlatButton
                        label="Continue"
                        onClick={() => {
                            this.props.actions.saveProductProperties();
                        }}
                        disabled={!isEnabled}
                    />
                </CardActions>
            </Card>
        );
    }
}


const mapStateToProps = state => {
    const {productProperties} = state;
    const isEnabled = productProperties.length >= 4;
    return {
        productProperties,
        isEnabled,
    };
};

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(Actions, dispatch)
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ContinueButton);
