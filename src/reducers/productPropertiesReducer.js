import * as types from '../constants/ActionTypes'
import {addNodeUnderParent, changeNodeAtPath, removeNodeAtPath} from 'react-sortable-tree';

const initialState = [
    {
        title: 'Chicken',
        children: [
            {
                title: 'Egg',
                value: '11'
            }
        ]
    }
];

const getNodeKey = ({treeIndex}) => treeIndex;

export default function productPropertiesReducer(state = initialState, action) {
    switch (action.type) {

        case types.PRODUCT_PROPERTIES_ADD_NODE:
            const {parentKey, newNode} = action;
            return addNodeUnderParent({
                treeData: state,
                parentKey,
                expandParent: true,
                getNodeKey,
                newNode,
            }).treeData;

        case types.PRODUCT_PROPERTIES_SET_TREE:
            const {tree} = action;
            return tree;

        case types.PRODUCT_PROPERTIES_DEL_NODE: {
            const {path} = action;
            return removeNodeAtPath({
                treeData: state,
                path,
                getNodeKey,
            });
        }

        case types.PRODUCT_PROPERTIES_UPD_NODE: {
            const {path, newNode} = action;
            return changeNodeAtPath({
                treeData: state,
                path,
                getNodeKey,
                newNode,
            });
        }

        case types.PRODUCT_PROPERTIES_NEW_MAIN: {
            const {newMain} = action;
            return state.concat(newMain);
        }

        default:
            return state
    }
}
