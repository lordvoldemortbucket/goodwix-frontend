import {combineReducers} from 'redux'
import productPropertiesReducer from './productPropertiesReducer'

const rootReducer = combineReducers({
    productProperties: productPropertiesReducer,
});

export default rootReducer;
