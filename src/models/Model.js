
class Model {

    static get(url) {
        return Model.fetch(url);
    }

    static post(url, data) {
        const headers = new Headers();
        headers.append("Content-Type", "application/json");
        headers.append("Access-Control-Allow-Origin", "http://localhost:3000");
        headers.append("Accept", "application/json");

        const jsonEncoded = JSON.stringify(data);

        const conf = {
            headers,
            mode: 'no-cors',
            method: "POST",
            body: jsonEncoded
        };
        return Model.fetch(url, conf);
    }

    static fetch(url, conf) {
        return new Promise((resolve, reject) => {
            fetch(url, conf)
                .then((response) => {
                    if (!response.ok) {
                        throw Error(response.statusText);
                    }
                    return response;
                })
                .then((response) => response.json())
                .then(resolve)
                .catch(reject);
        });
    }

}

export default Model;
