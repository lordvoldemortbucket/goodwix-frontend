import Model from './Model';
import config from '../config';

class ProductProperty extends Model {

    static get URL() {
        return `${config.API_URL}${config.API_PRODUCT_PROPERTIES}`;
    }

    static async getIndex() {
        try {
            return await ProductProperty.get(this.URL);
        } catch (e) {
            console.error(e);
        }
    }

    static async save(data) {
        try {
            return await ProductProperty.post(this.URL, data);
        } catch (e) {
            console.error(e);
        }
    }

}

export default ProductProperty;
