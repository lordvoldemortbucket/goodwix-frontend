import React from 'react'
import PropTypes from 'prop-types'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import * as Actions from '../actions/productProperties';
import ProductOptions from "../components/product-properties/ProductOptions";

const App = ({productProperties, actions}) => (
    <div>
`        <ProductOptions/>
    </div>
);

App.propTypes = {
    productProperties: PropTypes.array.isRequired,
    actions: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    productProperties: state.productProperties,
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(Actions, dispatch),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App);
