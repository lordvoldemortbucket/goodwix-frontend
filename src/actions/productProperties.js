import * as types from '../constants/ActionTypes'
import ProductProperty from "../models/ProductProperty";

export const addTodo = (parentKey, newNode) => ({type: types.PRODUCT_PROPERTIES_ADD_NODE, parentKey, newNode});
export const setTree = (tree) => ({type: types.PRODUCT_PROPERTIES_SET_TREE, tree});
export const delNode = (path) => ({type: types.PRODUCT_PROPERTIES_DEL_NODE, path});
export const updNode = (path, newNode) => ({type: types.PRODUCT_PROPERTIES_UPD_NODE, path, newNode});
export const newMain = (newMain) => ({type: types.PRODUCT_PROPERTIES_NEW_MAIN, newMain});

export function saveProductProperties() {
    return async (dispatch, getState) => {
        try {
            const {productProperties} = getState();
            await ProductProperty.save(productProperties);
        } catch (e) {
            console.error(e);
        }
    };
}